module("Set")

test("add", function() {
  var a = new Set();
  _u.each([1, 2, 3, 5, 9], a.add, a);
  deepEqual(a.values, [1, 2, 3, 5, 9]);
  _u.each([1, 2, 3, 5, 9], a.add, a);
  deepEqual(a.values, [1, 2, 3, 5, 9], "No new elements should added");

  a.values = [];
  _u.each([1, 1, 2, 3, 4, 4, 4, 6, 7], a.add, a);
  deepEqual(a.values, [1, 2, 3, 4, 6, 7]);

});

test("del", function() {
  var a = new Set();
  _u.each([1, 2, 3, 5, 9], a.add, a);
  _u.each([1, 2, 3, 9, 9], a.del, a);
  deepEqual(a.values, [5]);

});
test("contains", function() {
  var a = new Set();
  _u.each([1, 2, 3, 8, 9, 11], a.add, a);

  ok(a.contains(1));
  ok(a.contains(2));
  ok(a.contains(8));
  equal(a.contains(10), false);
  equal(a.contains({
    a: 1
  }), false);
  equal(a.contains([1, 2, 3, 8, 9, 11]), false);


});

test("subset-superset", function() {
  var a = new Set();
  var b = new Set();
  a.addMore(1, 2, 3);
  b.addMore(2, 3);
  ok(b.isSubsetOf(a));
  ok(a.isSupersetOf(b));
  equal(a.isSubsetOf(b), false);
  equal(b.isSupersetOf(a), false);
});

test("union", function() {
  var a = new Set();
  var b = new Set();
  a.addMore(1, 2, 3);
  b.addMore(3, 4, 5);
  deepEqual(a.union(b).values, [1, 2, 3, 4, 5]);
  deepEqual(b.union(a).values, [1, 2, 3, 4, 5]);
  deepEqual(Set.union(a, b).values, [1, 2, 3, 4, 5]);

});

test("intersect", function() {
  var a = new Set();
  var b = new Set();
  a.addMore(1, 2, 3);
  b.addMore(3, 4, 5);
  deepEqual(a.intersect(b).values, [3]);
  deepEqual(b.intersect(a).values, [3]);
  deepEqual(Set.intersect(a, b).values, [3]);
});

test("difference", function() {
  var a = new Set();
  var b = new Set();
  a.addMore(1, 2, 3);
  b.addMore(3, 4, 5);
  deepEqual(a.difference(b).values, [1, 2]);
  deepEqual(b.difference(a).values, [4, 5]);

});
test("Cartesian", function() {
  var a = new Set();
  var i,j,k,l;
  a.addMore(1, 0);
  deepEqual(Set.cartesian(a, a).values, [
    [0, 0],
    [0, 1],
    [1, 0],
    [1, 1]
  ],"Binary cartesian");
  var tripleres = [];
  for (i = 0; i < 2; i++)
    for (j = 0; j < 2; j++)
      for (k = 0; k < 2; k++) tripleres[4 * i + 2 * j + k] = [i, j, k];
  deepEqual(Set.cartesian(a, a, a).values, tripleres, "3 bit");
  var halfbyte = [];
  for (i = 0; i < 2; i++)
    for (j = 0; j < 2; j++)
      for (k = 0; k < 2; k++)
        for (l = 0; l < 2; l++) halfbyte[8 * i + 4 * j + 2 * k + l] = [i, j, k, l];
  deepEqual(Set.cartesian(a,a,a,a).values,halfbyte,"4 bit");
  
});