module("Tape");
test("misc", function() {
  var set = new Set(function(e) {
    return _u.isString(e) && e.length === 1;
  });
  set.addMore(_u.charRange('a', 'z'));
  set.add("-");
  var t = new Tape(set,"-");
  t.init("proba");
  deepEqual(t.dump(), "proba".split(""),"proba");
  t.movr(7); t.write("a");
  deepEqual(t.dump(),['p','r','o','b','a','-','-','a']);
});