module("Fsa");
test("states",function(){
  var m = new Fsa();
  m.addState('q0');
  m.addState('q1');
  m.addState('qs');
  deepEqual(m.states.values, ['q0','q1','qs'],"Add()");
  m.addState('q0');
  deepEqual(m.states.values, ['q0','q1','qs'],"Add(): Duplicated state should throttled");
  m.delState('q1');
  deepEqual(m.states.values, ['q0','qs'],"Del()");
  m.delState('q0');
  deepEqual(m.states.values, ['qs'],"Del()");

});

test("symbols",function(){
  var m = new Fsa();
  m.addSymbol('0');
  m.addSymbol('1');
  deepEqual(m.tape.base.values, ['0','1','eps'],"'0','1','eps'");
  ok(m.tape.base.contains('0'));
  ok(m.tape.base.contains('1'));
  m.delSymbol('0');
  equal(m.tape.base.contains('0'),false,"Deleted");
  ok(m.tape.base.contains('1'));
  deepEqual(m.tape.base.values, ['1','eps'],"'1','eps'");
});

test("finish states",function(){
  var m = new Fsa();
  m.addState('q0');
  m.addState('q1');
  m.addFinishState('q1');
  deepEqual(m.finishStates.values,['q1'],'dummy');
  m.delFinishState('q1');
  deepEqual(m.finishStates.values,[],'should be empty');
  throws(function(){m.addFinishState('q5')},"nonexistent states");
});

test("start state",function(){
  var m = new Fsa();
  m.addState('q');
  m.setStartState('q');
  equal(m.startState,'q','dummy');
  throws(function(){m.setStartState('w')});
});

test("exec",function(){
  function accpets(input){
    var res = m.calculate(input);
    if (res) {
      return true;
    }else{
      return false;
    }
  }
  var m = new FsaPlatform(); // harommal oszthato binaris szamok
  m.machine.addState("qs");
  m.machine.addState("q1");
  m.machine.addState("q2");

  m.machine.addSymbol("0");
  m.machine.addSymbol("1");

  m.machine.setStartState("qs");
  m.machine.addFinishState("qs");

  m.machine.addRule("qs","0","qs");
  m.machine.addRule("qs","1","q1");
  m.machine.addRule("q1","0","q2");
  m.machine.addRule("q1","1","qs");
  m.machine.addRule("q2","0","q1");
  m.machine.addRule("q2","1","q2");

  var expectedResult = [];
  var result = [];
  var i,data,str;
  for (i = 0; i < 20; i++) {
    data = i.toString(2);
    result[i] = accpets(data);
    expectedResult[i] = (i % 3 === 0? true : false);
    str = data + (expectedResult[i] ? " should accept." : "should reject.");
    equal(result[i],expectedResult[i],str);
  };


});