module("Tm");
test("binsucc", function() {
  function getOutput(input) {
    var chain = binarySucc.calculate(input);
    var i;
    binarySucc.initialize(input);
    var cfg;
    for (i = 1; i < chain.length; i++) {
      cfg = binarySucc.execute(chain[i]);
    }
    return cfg.tapes[0].data.join("");
  };
  var binarySucc = new TmPlatform();
  binarySucc.machine.addState("q0");
  binarySucc.machine.addState("q1");
  binarySucc.machine.addState("q2");
  binarySucc.machine.addState("qf");
  binarySucc.machine.setStartState("q0");
  binarySucc.machine.addFinishState("qf");

  binarySucc.machine.addSymbol("1");
  binarySucc.machine.addSymbol("0");

  binarySucc.machine.addRule("q0", "", "q1", "", Tape.LEFT);
  binarySucc.machine.addRule("q0", "0", "q0", "0", Tape.RIGHT);
  binarySucc.machine.addRule("q0", "1", "q0", "1", Tape.RIGHT);

  binarySucc.machine.addRule("q1", "", "q2", "1", Tape.RIGHT);
  binarySucc.machine.addRule("q1", "0", "q2", "1", Tape.LEFT);
  binarySucc.machine.addRule("q1", "1", "q1", "0", Tape.LEFT);

  binarySucc.machine.addRule("q2", "", "qf", "", Tape.LEFT);
  binarySucc.machine.addRule("q2", "0", "q2", "0", Tape.RIGHT);
  binarySucc.machine.addRule("q2", "1", "q2", "1", Tape.RIGHT);

  deepEqual(getOutput("1"), "10");  
  deepEqual(getOutput("10"), "11");
  deepEqual(getOutput("11"), "100");
  deepEqual(getOutput("100"), "101");
  deepEqual(getOutput("101"), "110");
  deepEqual(getOutput("110"), "111");
  deepEqual(getOutput("111"), "1000");
  deepEqual(getOutput("1000"), "1001");
  deepEqual(getOutput("1001"), "1010");
  deepEqual(getOutput("1010"), "1011");
  deepEqual(getOutput("1011"), "1100");
  deepEqual(getOutput("1100"), "1101");
  deepEqual(getOutput("1101"), "1110");
  deepEqual(getOutput("1110"), "1111");
  deepEqual(getOutput("1111"), "10000");
  deepEqual(getOutput("10000"), "10001");
  deepEqual(getOutput("10001"), "10010");
  deepEqual(getOutput("10010"), "10011");
  deepEqual(getOutput("10011"), "10100");
  deepEqual(getOutput("10100"), "10101");
  deepEqual(getOutput("10101"), "10110");
  deepEqual(getOutput("10110"), "10111");
  deepEqual(getOutput("10111"), "11000");
  deepEqual(getOutput("11000"), "11001");
  deepEqual(getOutput("11001"), "11010");
  deepEqual(getOutput("11010"), "11011");
  deepEqual(getOutput("11011"), "11100");
  deepEqual(getOutput("11100"), "11101");
  deepEqual(getOutput("11101"), "11110");

});