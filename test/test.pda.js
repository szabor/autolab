module("Pda");
test("execution", function() {
  function cfg(arr1, ptr1) {
    return {
      aState: 'q',
      tapes: [{
        data: ["a","a","a"],
        ptr: 1
      }, {
        data: arr1,
        ptr: ptr1
      }],
    };
  };
  var a = new Pda();
  a.addState('q');
  a.setStartState('q');
  a.setStartStackSymbol('Z0');
  a.addStackSymbol('1');
  a.addSymbol('a');

  var p = new PdaPlatform(a);
  p.initialize('aaa');
  a.addRule('q', 'a', 'Z0', 'q', 'Z0');
  a.addRule('q', 'a', 'Z0', 'q', ['Z0']);
  a.addRule('q', 'a', 'Z0', 'q', ['Z0', '1']);


  a.addRule('q', 'a', 'eps', 'q', 'Z0');
  a.addRule('q', 'a', 'eps', 'q', ['Z0']);
  a.addRule('q', 'a', 'eps', 'q', ['Z0', '1']);

  a.addRule('q', 'a', 'eps', 'q', 'eps');
  a.addRule('q', 'a', 'eps', 'q', ['eps']);
  a.addRule('q', 'a', 'eps', 'q', []);
  var r = _u.pluck(p.getApplicableRules(), "___id");

  var nextCfgs = _u.map(r, function(e) {
    return p.nextConfig(e);
  });

  var expectedCfgs = [];
  expectedCfgs[0]  = cfg(['Z0'],0);
  expectedCfgs[1]  = cfg(['Z0'],0);
  expectedCfgs[2]  = cfg(['Z0','1'],1);


  expectedCfgs[3]  = cfg(['Z0','Z0'],1);
  expectedCfgs[4]  = cfg(['Z0','Z0'],1);
  expectedCfgs[5]  = cfg(['Z0','Z0','1'],2);
  expectedCfgs[6]  = cfg(['Z0'],0);
  expectedCfgs[7] = cfg(['Z0'],0);
  expectedCfgs[8] = cfg(['Z0'],0);
  for(var i=0; i<nextCfgs.length; i+=1){
    deepEqual(nextCfgs[i],expectedCfgs[i],"Case #"+i);
  }

});
test("real example",function(){
  var a = new Pda();
  a.addState("q0");
  a.addState("q1");
  a.addState("qv");

  a.addSymbol("a");
  a.addSymbol("b");

  a.addStackSymbol("1");
  a.addStackSymbol("Z0");

  a.setStartState("q0");
  a.setStartStackSymbol("Z0");

  a.addFinishState("qv");

  a.addRule("q0","a","Z0","q0",["Z0","1"]);
  a.addRule("q0","a","1","q0",["1","1"]);
  a.addRule("q0","b","1","q1",["eps"]);
  a.addRule("q1","b","1","q1",["eps"]);
  a.addRule("q1","eps","Z0","qv",["Z0"]);
  a.setMode(Pda.EMPTYMODE);
  var p = new PdaPlatform(a);
  var data      = ["aaabbb","aabb","ab","aaabb","aabbb","aaa","bbb"];
  var expResult = [true,    true,  true,false,  false,  false,false];
  
  var result = [];
  result[0] = p.calculate(data[0]) ? true : false;
  result[1] = p.calculate(data[1]) ? true : false;
  result[2] = p.calculate(data[2]) ? true : false;
  result[3] = p.calculate(data[3]) ? true : false;
  result[4] = p.calculate(data[4]) ? true : false;
  result[5] = p.calculate(data[5]) ? true : false;
  /*_u.each(result,function(res,i){
    equal(res,expResult[i]);
  })*/
deepEqual(result[0],expResult[0]);
deepEqual(result[1],expResult[1]);
deepEqual(result[2],expResult[2]);
deepEqual(result[3],expResult[3]);
deepEqual(result[4],expResult[4]);
deepEqual(result[5],expResult[5]);

});