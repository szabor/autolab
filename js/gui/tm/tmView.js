var TmView = {
  init: function() {
    this.editBtns = {};
    this.editBtns.active = "";
    this.editBtns.all = $(".editorBtn");
    this.editBtns.addStateButton = $("#addStateBtn");
    this.editBtns.delStateButton = $("#delStateBtn");
    this.editBtns.setStartStateButton = $("#setStartStateBtn");
    this.editBtns.addRuleButton = $("#addRuleBtn");
    this.editBtns.delRuleButton = $("#delRuleBtn");
    this.editBtns.setFinishStateButton = $("#setFinishStateBtn");

    this.permBtns = {};
    this.permBtns.openButton = $("#openBtn");
    this.permBtns.saveButton = $("#saveBtn");
    this.input = {};
    this.input.word = $("#inputField");
    this.input.separator = $("#inputSeparator");
    this.input.word.val("");

    this.summaryTable = {};
    this.summaryTable.tables = {};
    this.summaryTable.tables.states = $("#states-body");
    this.summaryTable.tables.symbols = $("#symbols-body");
    this.summaryTable.tables.finishStates = $("#finishStates-body");
    this.summaryTable.tables.rules = $("#rules-table");

    this.summaryTable.counters = {};
    this.summaryTable.counters.states = $("#state-count");
    this.summaryTable.counters.symbols = $("#symbol-count");
    this.summaryTable.counters.finishStates = $("#finishState-count");
    this.summaryTable.counters.rules = $("#rule-count");

    this.simTable = {};
    this.simTable.tapeTable = $("#tapeTable");
    this.simTable.nextsTable = $("#simNexts");
    this.simTable.container = $("#simInfoContainer");
    this.simTable.globSucc = $("#globSucc");
    this.simTable.actSucc = $("#actSucc");

    this.autoplay = {};
    this.autoplay.startBtn = $("#startAuto");
    this.autoplay.stopBtn = $("#pauseAuto");

    this.ruleForm = {};
    this.ruleForm.modal = $("#addRuleModal");
    this.ruleForm.aState = $("#addRuleModal-src");
    this.ruleForm.rSymbol = $("#addRuleModal-read");
    this.ruleForm.wSymbol = $("#addRuleModal-write");
    this.ruleForm.nState = $("#addRuleModal-dest");
    this.ruleForm.direction = $("#addRuleModal-direction");
    this.ruleForm.btn = $("#addRuleModalBtn");

    if (_u.isUndefined(this.sketch)) {
      this.sketch = {};
    }
    this.sketch.bounds = {
      width: $("#papercontainer").width(),
      height: $(window).height() * 0.7
    };
    if (this.sketch.graph) {
      this.sketch.graph.resetCells();
    } else {
      this.sketch.graph = new joint.dia.Graph;
      this.sketch.paper = new joint.dia.Paper({
        el: $('#paper'),
        width: this.sketch.bounds.width,
        height: this.sketch.bounds.height,
        gridSize: 1,
        model: this.sketch.graph
      });
    };
    _u.each(this.summaryTable.tables, function(table) {
      table.children().remove();
    }, this);

    _u.each(this.summaryTable.counters, function(counter) {
      counter.text("0");
    }, this);
  },
  getInput: function() {
    return this.input.word.val();

  },
  getSeparator: function() {
    return this.input.separator.val();
  },
  disable: function(elem) {
    elem.attr("disabled", "disabled");
  },
  enable: function(elem) {
    elem.removeAttr("disabled");
  },
  disableEditor: function(willAccept) {
    this.editBtns.all.attr("disabled", "disabled");
    $("#initBtn span").removeClass("glyphicon-start");
    $("#initBtn span").addClass("glyphicon-stop");
    $(".editBtn").attr("disabled", "disabled");
    this.simTable.container.show();
    this.simTable.globSucc.children().remove();
    var markup;
    if (willAccept) {
      markup = '<span class="label label-success">Elfogadja</span>';
    } else {
      markup = '<span class="label label-danger">Elutasítja</span>';
    }
    this.simTable.globSucc.append($(markup));
    //$("#")
  },
  enableEditor: function() {
    this.editBtns.all.removeAttr("disabled");
    $(".editBtn").removeAttr("disabled");
    $("#initBtn span").removeClass("glyphicon-stop");
    $("#initBtn span").addClass("glyphicon-start");
    $("#tapeTable").children().remove();
    $("#simNexts").children().remove();

    this.simTable.container.hide();
    if (this.aActiveState) {
      this.delHLState(this.aActiveState);
      this.aActiveState = undefined;
    }

  },
  toggleEditBtn: function(btn) {
    if (this.editBtns.active) {
      this.editBtns.active.removeClass("active");
    }
    if (btn) {
      this.editBtns.active = $("#" + btn);
      this.editBtns.active.addClass("active");

    }
  },
  renderRuleForm: function(src, dest) {
    this.ruleForm.aState.val(src);
    this.ruleForm.nState.val(dest);
    this.ruleForm.rSymbol.val("");
    this.ruleForm.wSymbol.val("");
    this.ruleForm.direction.val("right");
    this.ruleForm.modal.modal();


  },
  getRuleArgs: function() {
    return [
      this.ruleForm.aState.val(),
      this.ruleForm.rSymbol.val(),
      this.ruleForm.nState.val(),
      this.ruleForm.wSymbol.val(),
      this.ruleForm.direction.val()
    ];
  },
  getNameByCell: function(cell) {
    return cell.attributes.attrs.text.text;
  },
  updateCounter: function(name, val, rel) {
    var elem = this.summaryTable.counters[name];
    if (rel) {
      val = val + parseInt(elem.text())
    }
    elem.text(val.toString());
  },
  addState: function(state, x, y, btnId) {

    var markup = '<button id=' + btnId + ' class="editBtn">' + state + ' <span class="glyphicon glyphicon-ban-circle"></span></button>';
    this.summaryTable.tables.states.append($(markup));
    this.updateCounter("states", 1, true);
    var cell = new joint.shapes.fsa.MixState({
      position: {
        x: x - 20,
        y: y - 20
      },
      size: {
        width: 60,
        height: 60
      },
      attrs: {
        text: {
          text: state
        }
      }
    });
    this.sketch.graph.addCell(cell);
    return cell;
  },
  renderSimStep: function(aCfg, rules, cfgs, isAccepting) {
    this.disable(this.autoplay.startBtn);
    this.disable(this.autoplay.stopBtn);
    this.renderActualCfg(aCfg, isAccepting);
    this.renderNextCfgs(_u.zip(rules, cfgs));
  },
  renderActualCfg: function(aCfg, accept) {
    this.setActualState(aCfg.aState);
    var ptrRow = "<tr>";
    var dataRow;
    var i;
    this.simTable.tapeTable.children().remove();
    for (i = 0; i < aCfg.tapes[0].ptr; i += 1) {
      ptrRow += "<td></td>";
    };
    ptrRow += '<td><span class="glyphicon glyphicon-arrow-down"></td>';
    for (i = aCfg.tapes[0].ptr + 1; i < aCfg.tapes[0].data.length; i += 1) {
      ptrRow += "<td></td>";
    };
    ptrRow += "</tr>";
    this.simTable.tapeTable.append($(ptrRow));

    dataRow = _u.reduce(aCfg.tapes[0].data, function(memo, symbol) {
      return memo + "<td>" + (symbol === "eps" ? "ε" : symbol) + "</td>";
    }, "<tr>");
    dataRow += "</tr>";
    this.simTable.tapeTable.append($(dataRow));
    this.simTable.actSucc.children().remove();
    var markup;
    if (accept) {
      markup = '<span class="label label-info">Elfogadó</span>';
    } else {
      markup = '<span class="label label-warning">Nem elfogadó</span>';
    }
    this.simTable.actSucc.append($(markup));

  },
  hlSimRow: function(rid) {
    this.enable(this.autoplay.startBtn);
    this.enable(this.autoplay.stopBtn);
    $("#simRow_" + rid).addClass("success");
  },
  genRuleLabel: function(readSymbol, writeSymbol, direction) {
    if (readSymbol === "") readSymbol = '☐';
    if (writeSymbol === "") writeSymbol = '☐';
    if (direction === Tape.RIGHT) {
      direction = "⟶";
    } else if (direction === Tape.LEFT) {
      direction = "⟵";
    } else if (direction === Tape.HOLD) {
      direction = "ꜜ";
    }
    return readSymbol + "/" + writeSymbol + " " + direction;

  },
  genRuleMarkup: function(aState, readSymbol, nState, writeSymbol, direction) {
    if (readSymbol === "") readSymbol = '☐';
    if (writeSymbol === "") writeSymbol = '☐';
    if (direction === Tape.RIGHT) {
      direction = "⟶";
    } else if (direction === Tape.LEFT) {
      direction = "⟵";
    } else if (direction === Tape.HOLD) {
      direction = "ꜜ";
    }
    return '&delta;(' + aState + ', ' + readSymbol + ') = (' + nState + ', ' + writeSymbol + ', ' + direction + ')';
  },
  renderNextCfgs: function(rows) {
    this.simTable.nextsTable.children().remove();
    var header;
    if (rows.length > 0) {
      header = '<th>Szabály</th><th>Konfiguráció</th><th></th>';
      this.simTable.nextsTable.append($(header));
      _u.each(rows, function(row) {
        var rule = row[0];
        var cfg = row[1];
        var rowId = "simRow_" + rule.___id;
        var direction = rule.move === Tape.LEFT ? "⟵" : "⟶";

        var ruleMarkup = '<td> ' + this.genRuleMarkup(rule.aState, rule.readSymbol, rule.nState, rule.writeSymbol, rule.move) + '</td>';
        var btnMarkup = '<td><button class="nextCfg" id="nextCfg_' + rule.___id + '"><span class="glyphicon glyphicon-step-forward"></span></button></td>';
        var markup = '<tr id="' + rowId + '">' + ruleMarkup + btnMarkup + '</tr>';
        this.simTable.nextsTable.append($(markup));

        this.simTable

      }, this);
    } else {
      header = '<th>Nincs megfelelő szabály a fojtatáshoz</th>';
      this.simTable.nextsTable.append($(header));
    }
  },
  setActualState: function(cell) {
    if (this.aActiveState) {
      this.delHLState(this.aActiveState);
    }
    this.aActiveState = cell;
    if (cell) this.addHLState(cell, "#A0FFA0");
  },
  addHLState: function(cell, color) {
    cell.attr({
      ".inner": {
        "fill": color || "#A0A0FF",
      }
    });
  },
  delHLState: function(cell) {
    cell.attr({
      ".inner": {
        "fill": "#FFFFFF",
      }
    });
  },
  delState: function(cell, btnId) {
    cell.remove();
    $("#" + btnId).remove();
    this.updateCounter("states", -1, true);
    //TODO: counter, button
  },
  addSymbol: function(symbol, btnId) {
    var markup = '<button id="' + btnId + '"class="editBtn">' + symbol + ' <span class="glyphicon glyphicon-ban-circle"></span></button>'
    this.summaryTable.tables.symbols.append($(markup));
    this.updateCounter("symbols", 1, true);
  },
  delSymbol: function(btnId) {
    $("#" + btnId).remove();
    this.updateCounter("symbols", -1, true);

  },
  addFinishState: function(cell, btnId) {
    var name = this.getNameByCell(cell);
    var markup = '<button class="editBtn" id="' + btnId + '">' + name + ' <span class="glyphicon glyphicon-ban-circle"></span></button>';
    this.summaryTable.tables.finishStates.append($(markup));
    cell.attr({
      ".inner": {
        "stroke": "black"
      }
    });
    this.updateCounter("finishStates", 1, true);


  },
  delFinishState: function(cell, btnId) {
    $("#" + btnId).remove();
    cell.attr({
      ".inner": {
        "stroke": "white"
      }
    });
    this.updateCounter("finishStates", -1, true);

  },
  setStartState: function(newCell, oldCell) {
    if (oldCell) oldCell.attr({
      ".outer": {
        "stroke-dasharray": ""
      }
    }); //stroke-dasharray="10,10"
    if (newCell) newCell.attr({
      ".outer": {
        "stroke-dasharray": "10,10"
      }
    });

  },
  shrinkRules: function() {
    var links = this.sketch.getLinks();
  },
  setLabel: function(cell, label) {
    cell.label(0, {
      position: .5,
      attrs: {
        rect: {
          fill: 'white'
        },
        text: {
          text: label
        }
      }
    });
  },
  addRule: function(srcCell, rSymbol, destCell, wSymbol, direction, btnId, rowId, labels, ruleCellId) {
    var ruleCell;
    //ymbol = symbol === "eps" ? "ε" : symbol;
    //stackSymbol = stackSymbol === "eps" ? "ε" : stackSymbol;
    if (ruleCellId) {
      ruleCell = this.sketch.graph.getCell(ruleCellId);
    } else {
      ruleCell = new joint.shapes.fsa.Arrow({
        source: {
          id: srcCell.id
        },
        target: {
          id: destCell.id
        },
        labels: [{
          position: .5,
          attrs: {
            text: {
              text: "",
              'font-weight': 'bold'
            }
          }
        }],
        vertices: []

      });
      if (srcCell.id === destCell.id) {
        var x = srcCell.attributes.position.x + 30;
        var y = srcCell.attributes.position.y + 30;
        ruleCell.set('vertices', [{
          x: x - 30,
          y: y - 90
        }, {
          x: x + 30,
          y: y - 90
        }]);
      };
    }
    var labels = _u.reduce(labels, function(memo, e) {
      return memo + e + "\n";
    }, "");
    this.setLabel(ruleCell, labels);


    this.sketch.graph.addCell(ruleCell);
    var src = this.getNameByCell(srcCell);
    var dest = this.getNameByCell(destCell);
    var markup = '<tr id="' + rowId + '"><td>' + this.genRuleMarkup(src, rSymbol, dest, wSymbol, direction) + '</td>'
    markup += '<td>  <button id="' + btnId + '" class="editBtn"><span class="glyphicon glyphicon-ban-circle"></span></button></td>';
    this.summaryTable.tables.rules.append($(markup));
    this.updateCounter("rules", 1, true);
    return ruleCell;


  },
  delRule: function(cell, rowId, labels) {
    if (cell) { // ha allapottorles mellekhatasakent kerulunk ide, akkor a cell mar meg lett semmisitve..
      if (labels.length > 0) {
        labels = _u.reduce(labels, function(memo, e) {
          return memo + e + "\n";
        }, "");
        this.setLabel(cell, labels);
      } else {

        cell.remove();
      }
    }

    $("#" + rowId).remove();
    this.updateCounter("rules", -1, true);
  },

};