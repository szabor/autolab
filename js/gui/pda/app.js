String.prototype.hashCode = function() {
  var hash = 0,
    i, chr, len;
  if (this.length == 0) return hash;
  for (i = 0, len = this.length; i < len; i++) {
    chr = this.charCodeAt(i);
    hash = ((hash << 5) - hash) + chr;
    hash |= 0; // Convert to 32bit integer
  }
  return hash.toString();
};

function errorLogger(msg) {
  $("#errText").text(msg);
  $("#errModal").modal();
}

function init() {
  PdaView.init();
  PdaController.init(PdaView, new PdaPlatform());
}
$(function() {
  radio("error").subscribe(errorLogger);
  init();
  $("#newBtn").on("click", init);
  $("#loadBtn").off().on("click", function() {
    init();
    var source = $("#openSrcCode").val();
    PdaController.loadPda(source);
  });
});
$(window).resize(function(){
  PdaController.view.sketch.paper.setDimension($("#papercontainer").width(), $(window).height() * 0.7);
});