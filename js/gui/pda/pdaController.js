var PdaController = {
  init: function(view, platform) {
    this.view = view;
    this.platform = platform;
    this.mode = {};
    this.mode.EDIT = {};
    this.mode.SIM = {};
    //this.mode.mode = this.mode.EDIT;
    this.model = platform.machine;

    this.view.sketch.paper.off();

    this.view.sketch.paper.on('blank:pointerup', function(evt, x, y) {
      PdaController.blankClick(evt, x, y);
    });

    this.view.sketch.paper.on('cell:pointerup', function(evt, x, y) {
      PdaController.cellClick(evt, x, y);
    });

    this.ruleRegistry = TAFFY();
    this.stateRegistry = TAFFY();


    this.view.editBtns.all.off().on("click", function(evt) {
      PdaController.setCommand(evt.currentTarget.id);
    });

    this.view.simData.mode.off().on("change", function(evt) {
      var val = PdaView.simData.mode.val();
      if (val === "fs") {
        PdaController.model.setMode(Pda.FSMODE);
      } else if (val === "empty") {
        PdaController.model.setMode(Pda.EMPTYMODE);
      }
    });
    $("#saveBtn").off().on("click", function() {
      var dump = PdaController.dumpPda();
      $("#saveSrcCode").val(JSON.stringify(dump, undefined, 2));
      $("#saveModal").modal();
    });
    $("#openBtn").off().on("click", function() {
      $("#openSrcCode").val("");
      $("#openModal").modal();
    });


    function startSimulator() {

      var succ = PdaController.setMode(PdaController.mode.SIM);
      if (succ) {
        PdaController.fetchConfig();
        $("#initBtn").off().on("click", startEditor);
      }
    };

    function startEditor() {
      $("#initBtn").off().on("click", startSimulator);
      PdaController.setMode(PdaController.mode.EDIT);
    };

    this.view.ruleForm.btn.off().on("click", function() {
      var args = PdaController.view.getRuleArgs();
      PdaController.addRuleByForm.apply(PdaController, args);
    });
    startEditor();
    //this.setMode(this.mode.EDIT);
    this.addStackSymbol("#");
    this.model.setStartStackSymbol("#");
    $("#delStackSymbol_" + "#".hashCode()).off(); // ezt nem kell torolni..:)
    this.model.setMode(Pda.FSMODE);

    this.view.autoplay.startBtn.off().on("click", PdaController.startAutoplay);
    this.view.autoplay.stopBtn.off().on("click", PdaController.stopAutoplay);


  },
  stopAutoplay: function() {
    PdaController.view.autoplay.startBtn.off().on("click", PdaController.startAutoplay);
    clearInterval(PdaController.timer);
    PdaController.timer = undefined;
  },
  startAutoplay: function() {
    PdaController.view.autoplay.startBtn.off();
    PdaController.timer = setInterval(function() {
      var goodBtn = $("#simNexts .success button");
      if (goodBtn.length > 0) {
        goodBtn.trigger("click");
      } else {
        PdaController.stopAutoplay();
      }
    }, 800);
  },
  insertState: function(name, cell, hash) {
    hash = hash || name.hashCode();
    PdaController.stateRegistry.insert({
      name: name,
      cell: cell,
      hash: hash
    });
  },
  getStateByHash: function(hash) {
    return PdaController.stateRegistry({
      hash: hash
    }).get()[0];
  },
  getStateByName: function(name) {
    return PdaController.stateRegistry({
      name: name
    }).get()[0];
  },
  getStateByCell: function(cell) {
    var name = PdaController.view.getNameByCell(cell);
    return PdaController.getStateByName(name);
  },
  setMode: function(mode) {
    var success;
    var input;
    var separator;
    if (mode === this.mode.EDIT) {
      this.view.enableEditor();

    } else if (mode === this.mode.SIM) {
      input = this.view.getInput();
      separator = ""; //this.view.getSeparator();
      input = input.split(separator);
      success = this.platform.initialize(input);
      if (success) {

        var isOk = this.platform.calculate(input);
        this.view.disableEditor(isOk ? true : false);
        //this.fetchConfig();
        return true;
      } else {
        var causa;
        if (this.model.startState) {
          causa = "Az input valószínűleg ismeretlen szimbólumokat tartalmaz.";
        } else {
          causa = "A kezdőállapot nincs beállítva.";
        }
        radio("error").broadcast("Nem lehet elkezdeni a szimulációt: " + causa);
        return false;
      }



    }
  },
  fetchConfig: function() {
    var aCfg = this.platform.getConfig();
    var chain = this.platform.calculate(aCfg);
    var nRules = this.platform.getApplicableRules(aCfg);
    aCfg.aState = this.getStateByName(aCfg.aState).cell;
    var nCfgs = _u.map(nRules, function(rule) {
      return this.platform.nextConfig(rule.___id);
    }, this);
    this.view.renderSimStep(aCfg, nRules, nCfgs, this.platform.isAcceptable());
    if (!_u.isUndefined(chain[1])) {
      var nextGoodRule = chain[1];
      this.view.hlSimRow(nextGoodRule);
    }
    $(".nextCfg").on("click", function(evt) {
      var rid = evt.currentTarget.id.substr(8);
      PdaController.step(rid);
    });

  },
  step: function(rid) {
    this.platform.execute(rid);
    this.fetchConfig();
  },
  setCommand: function(id) {
    //var id = evt.currentTarget.id;
    this.view.toggleEditBtn(id);
    if (id) {
      this.command = id.substr(0, id.length - 3);
    } else {
      this.command = undefined;
    }
    if (this.command !== "addRule" && !_u.isUndefined(this.addRuleSrc)) {
      this.view.delHLState(this.addRuleSrc);
      this.addRuleSrc = undefined;
    }

    //this.command = command;
  },
  cellClick: function(evt, x, y) {
    //if (x < 0 || x > this.view.sketch.bounds.width || y < 0 || y > this.view.sketch.bounds.height) return false;

    var cell = evt.model;

    var handled = ['delState', 'setFinishState', 'setStartState', 'addRule'];
    if (handled.indexOf(this.command) !== -1 && cell instanceof joint.shapes.fsa.MixState) {
      //var name = (cell instanceof joint.dia.Element ? cell.attributes.attrs.text.text)
      this[this.command](cell);
      //if(this.command)this.setCommand();
    } else if (this.command === "delRule" && cell instanceof joint.shapes.fsa.Arrow) {
      this.delRule(cell);
    }
  },
  blankClick: function(evt, x, y) {
    if (x < 0 || x > this.view.sketch.bounds.width || y < 0 || y > this.view.sketch.bounds.height) return false;
    var handled = ['addState']
    if (handled.indexOf(this.command) !== -1) {
      this[this.command](x, y);
      this.setCommand();
    }
  },
  addState: function(x, y, name) {
    name = name || window.prompt("Új állapot neve: ");
    if (!_u.isNull(name)) {
      var res = this.model.addState(name);
      if (res) {
        var hash = name.hashCode();
        var cell = this.view.addState(name, x, y, "delState_" + hash);
        this.insertState(name, cell, hash);
        $("#delState_" + hash).on("click", function(evt) {
          var hash = evt.currentTarget.id.substr(9);
          var name = PdaController.getStateByHash(hash).name;
          PdaController.delState(name);
        });

      } else {
        radio("error").broadcast("Nem lehet hozzáadni a(z) '" + name + "' nevű állapot. Valószínűleg már létezik");
      }
      this.setCommand();
    }

  },
  delState: function(state) {
    var name;
    var cell;
    var hash;
    if (!_u.isString(state)) {
      name = this.view.getNameByCell(state);
      hash = this.getStateByName(name).hash;
      cell = state;
    } else {
      name = state;
      hash = this.getStateByName(name).hash;
      cell = this.getStateByName(name).cell;
    }
    if (this.model.isFinishState(name)) {
      this.setFinishState(name);
    }
    if (this.model.startState === name) {
      this.model.startState = undefined;
    }
    this.model.delState(name);

    this.view.delState(cell, "delState_" + hash);
    //ki kell torolni az erintett szabajokat is...
    var rules = this.model.rules([{
      aState: name
    }, {
      nState: name
    }]).get();

    _u.each(rules, function(rule) {
      //var graphId = this.ruleRegistry({modelId: rule.___id}).get()[0].graphId;
      this.delRule(rule.___id);
    }, this);
    this.setCommand();
  },
  setFinishState: function(state) {
    var name;
    var cell;
    var hash
    if (!_u.isString(state)) {
      name = this.view.getNameByCell(state);
      hash = this.getStateByName(name).hash;
      cell = state;
    } else {
      name = state;
      hash = this.getStateByName(name).hash;
      cell = this.getStateByName(name).cell;
    }
    var isFinishState = this.model.finishStates.contains(name);
    if (!isFinishState) {
      var success = this.model.addFinishState(name);
      if (success) {
        this.view.addFinishState(cell, "delFinishState_" + hash);
        $("#delFinishState_" + hash).on("click", function(evt) {
          var hash = evt.currentTarget.id.substr(15);
          var name = PdaController.getStateByHash(hash).name;
          PdaController.setFinishState(name);
        });
      }

    } else {
      this.model.delFinishState(name);
      this.view.delFinishState(cell, "delFinishState_" + hash);
    };
    this.setCommand();

  },
  setStartState: function(state) {
    var name;
    var cell;
    var hash;
    if (!_u.isString(state)) {
      name = this.view.getNameByCell(state);
      hash = this.getStateByName(name).hash;
      cell = state;
    } else {
      name = state;
      hash = this.getStateByName(name).hash;
      cell = this.getStateByName(name).cell;
    }
    var oldStart = this.model.startState;
    var oldCell = oldStart ? this.getStateByName(oldStart).cell : undefined;
    var success = this.model.setStartState(name);
    if (success) {
      this.view.setStartState(cell, oldCell);
    }
    this.setCommand();
  },
  addRuleByForm: function(aState, rSymbol, rStackSymbol, nState, toPush) {
    var errMsg;
    try {
      if (!this.model.states.contains(aState)) {
        throw new Error('A beírt állapot nem található: "' + aState + '"');
      }
      if (!this.model.states.contains(nState)) {
        throw new Error('A beírt állapot nem található: "' + nState + '"');
      }
      if (rSymbol === "") {
        rSymbol = "eps";
      }
      if (rStackSymbol === "") {
        rStackSymbol = "eps";
      }
      var aCell = this.getStateByName(aState).cell;
      var nCell = this.getStateByName(nState).cell;

      var success = false;
      if (_u.isString(toPush)) {
        toPush = toPush.split(" ");
      }
      if (toPush.length === 0 || (toPush.length === 1 && toPush[0] === "")) {
        toPush = ['eps'];
      }

      if (!this.model.tapes[0].base.contains(rSymbol)) {
        if (!this.addSymbol(rSymbol)) {
          return false;
        }
      };
      if (!this.model.tapes[1].base.contains(rStackSymbol)) {
        if (this.addStackSymbol(rStackSymbol)) {
          return false;
        }
      };
      try {
        _u.each(toPush, function(symbol) {
          if (!this.model.tapes[1].base.contains(symbol)) {
            if (!this.addStackSymbol(symbol)) {
              throw new Error();
            }
          };
        }, this);

      } catch (e) {
        return false;
      }
      modelId = this.model.addRule(aState, rSymbol, rStackSymbol, nState, toPush);
      if (modelId) {
        var cellId;
        var rules = this.model.rules({
          aState: aState,
          nState: nState
        }).get();
        if (rules.length > 1) {
          cellId = this.ruleRegistry({
            modelId: rules[0].___id
          }).get()[0].graphId;
        }
        //rules = _u.pluck(rules,)
        var labels = _u.map(rules, function(e) {
          return this.genRuleLabel(e.readSymbol, e.readStackSymbol, e.writeStackSymbol);
        }, this.view);

        var link = this.view.addRule(aCell, rSymbol, rStackSymbol, nCell, toPush, "delRule_" + modelId, "ruleRow_" + modelId, labels, cellId);
        this.ruleRegistry.insert({
          modelId: modelId,
          graphId: link.id
        });
        $("#delRule_" + modelId).on("click", function(evt) {
          PdaController.delRule(evt.currentTarget.id.substr(8));
        });
      } else {
        throw new Error("A szabályt nem sikerült hozzáadni");
      }


      return link;
    } catch (e) {
      radio("error").broadcast(e);
      return false;
    };

  },
  addStackSymbol: function(symbol) {
    var succ = this.model.addStackSymbol(symbol);
    if (succ) {
      this.view.addStackSymbol(symbol, "delStackSymbol_" + symbol.hashCode());
      $("#delStackSymbol_" + symbol.hashCode()).on("click", function(evt) {
        PdaController.delStackSymbol(symbol); // scoping magic :)
        return true;
      });
    } else {
      radio("error").broadcast("Nem sikerült hozzáadni a veremszimbólumot!");
      return false;
    }

  },
  delStackSymbol: function(symbol) {
    var succ = this.model.delStackSymbol(symbol);
    if (succ) {
      this.view.delStackSymbol("delStackSymbol_" + symbol.hashCode());
      var affectedRules = this.model.rules(function() {
        return (this.readStackSymbol === symbol || this.writeStackSymbol.indexOf(symbol) >= 0);
      }).get();


      _u.each(affectedRules, function(rule) {
        this.delRule(rule.___id);
      }, this);
    }

  },
  addRule: function(cell) {
    var name = this.view.getNameByCell(cell);
    if (_u.isUndefined(this.addRuleSrc)) {
      this.addRuleSrc = name;
      this.view.setActualState(cell);
    } else {
      this.view.renderRuleForm(this.addRuleSrc, name);
      this.addRuleSrc = undefined;
      this.setCommand();
      this.view.setActualState();
    }
  },



  delRule: function(rule) {
    var modelId;
    var graphId;
    var cell;
    if (_u.isString(rule)) {
      modelId = rule;
      graphId = this.ruleRegistry({
        modelId: rule
      }).get()[0].graphId;
      cell = this.view.sketch.graph.getCell(graphId);
    } else {
      cell = rule;
      graphId = cell.id;
      modelId = this.ruleRegistry({
        graphId: graphId
      }).get()[0].modelId;
    }
    var mRule = this.model.rules({
      ___id: modelId
    }).get()[0];
    this.model.rules({
      ___id: modelId
    }).remove();
    var aState = mRule.aState;
    var nState = mRule.nState;
    this.model.delRule(mRule.___id);
    var rules = this.model.rules({
      aState: aState,
      nState: nState
    }).get();
    var labels = _u.map(rules, function(e) {
      return this.view.genRuleLabel(e.readSymbol, e.readStackSymbol, e.writeStackSymbol);
    }, this);
    this.view.delRule(cell, "ruleRow_" + modelId, labels);
    this.setCommand();
  },

  addSymbol: function(symbol) {
    var succ = this.model.addSymbol(symbol);
    if (succ) {
      this.view.addSymbol(symbol, "delSymbol_" + symbol.hashCode());
      $("#delSymbol_" + symbol.hashCode()).on("click", function(evt) {
        PdaController.delSymbol(symbol);
      });
      return true;
    } else {
      radio("error").broadcast("Nem sikerült hozzáadni a szimbólumot");
      return false;
    }
  },
  delSymbol: function(symbol) {
    var succ = this.model.delSymbol(symbol);
    if (succ) {
      this.view.delSymbol("delSymbol_" + symbol.hashCode());
      var affectedRules = this.model.rules({
        "readSymbol": symbol
      }).get();
      _u.each(affectedRules, function(rule) {
        this.delRule(rule.___id);
      }, this);
    }
  },
  dumpPda: function() {
    var res = this.model.dump();
    var cells = this.view.sketch.graph.getElements();
    res.states = _u.map(cells, function(cell) {
      return {
        "name": cell.attributes.attrs.text.text,
        "x": cell.attributes.position.x, //el kell tolni, mer a megjelenitoben alapbol el van ennyivel tolva
        "y": cell.attributes.position.y
      };
    });
    var links = this.view.sketch.graph.getLinks();
    res.stackSymbols = _u.reject(res.stackSymbols,function(e){
      return e==="#"
    });
    var rules = this.model.rules().get();
    res.rules = _u.map(rules, function(rule) {
      var linkId = this.ruleRegistry({
        modelId: rule.___id
      }).get()[0].graphId;
      var vertices = this.view.sketch.graph.getCell(linkId).attributes.vertices;
      return {
        aState: rule.aState,
        nState: rule.nState,
        symbol: rule.readSymbol,
        stackSymbol: rule.readStackSymbol,
        toPush: rule.writeStackSymbol,
        vertices: vertices,
      };

    }, this);

    return res;
  },
  loadPda: function(pdaDefinition) {
    if (_u.isString(pdaDefinition)) {
      try {
        pdaDefinition = JSON.parse(pdaDefinition);
      } catch (e) {
        radio("error").broadcast("Nem lehet megnyitni! Érvénytelen veremautomata");
        return false;
      }
    }
    if (_u.isUndefined(pdaDefinition.type) || pdaDefinition.type !== "Pda") {
      radio("error").broadcast("Nem sikerült megnyitni. Nem érvényes veremautomata");
      return false;
    }
    _u.each(pdaDefinition.states, function(state) {
      this.addState(state.x + 20, state.y + 20, state.name);
    }, this);

    _u.each(pdaDefinition.symbols, function(symbol) {
      this.addSymbol(symbol);
    }, this);

    _u.each(pdaDefinition.stackSymbols, function(symbol) {
      this.addStackSymbol(symbol);
    }, this);

    _u.each(pdaDefinition.finishStates, function(state) {
      this.setFinishState(state);
    }, this);
    if (pdaDefinition.mode === "fs") {
      this.model.setMode(Pda.FSMODE);
      this.view.simData.mode.val("fs");
    } else if (pdaDefinition.mode === "empty") {
      this.model.setMode(Pda.EMPTYMODE);
      this.view.simData.mode.val("empty");
    }
    if (pdaDefinition.startState) this.setStartState(pdaDefinition.startState);

    _u.each(pdaDefinition.rules, function(rule) {
      var aState = rule.aState;
      var nState = rule.nState;
      var symbol = rule.symbol;
      var stackSymbol = rule.stackSymbol;
      var toPush = rule.toPush;
      var link = this.addRuleByForm(aState, symbol, stackSymbol, nState, toPush);
      link.set('vertices', rule.vertices);

    }, this);
  }

};