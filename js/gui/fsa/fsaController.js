var FsaController = {
  init: function(view, platform) {
    this.view = view;
    this.platform = platform;
    this.mode = {};
    this.mode.EDIT = {};
    this.mode.SIM = {};
    //this.mode.mode = this.mode.EDIT;
    this.model = platform.machine;
    this.view.sketch.paper.off();

    this.view.sketch.paper.on('blank:pointerup', function(evt, x, y) {
      FsaController.blankClick(evt, x, y);
    });

    this.view.sketch.paper.on('cell:pointerup', function(evt, x, y) {
      FsaController.cellClick(evt, x, y);
    });

    this.ruleRegistry = TAFFY();
    this.stateRegistry = TAFFY();

    this.view.editBtns.all.on("click", function(evt) {
      FsaController.setCommand(evt.currentTarget.id);
    });
    $("#saveBtn").on("click", function() {
      var dump = FsaController.dumpFsa();
      $("#saveSrcCode").val(JSON.stringify(dump, undefined, 2));
      $("#saveModal").modal();
    });
    $("#openBtn").on("click", function() {
      $("#openSrcCode").val("");
      $("#openModal").modal();
    });
    /* $("#loadBtn").on("click", function() {
      FsaView.init();
      FsaController.init(FsaView, new FsaPlatform());
      var source = $("#srcCode").val();
      FsaController.loadFsa(source);
    });
*/


    this.view.autoplay.startBtn.off().on("click", FsaController.startAutoplay);
    this.view.autoplay.stopBtn.off().on("click", FsaController.stopAutoplay);

    function startSimulator() {

      var succ = FsaController.setMode(FsaController.mode.SIM);
      if (succ) {
        FsaController.fetchConfig();
        $("#initBtn").off().on("click", startEditor);
      }

    };

    function startEditor() {
      $("#initBtn").off().on("click", startSimulator);
      FsaController.setMode(FsaController.mode.EDIT);
    };

    startEditor();



  },
  stopAutoplay: function() {
    FsaController.view.autoplay.startBtn.off().on("click", FsaController.startAutoplay);
    clearInterval(FsaController.timer);
    FsaController.timer = undefined;
  },
  startAutoplay: function() {
    FsaController.view.autoplay.startBtn.off();
    FsaController.timer = setInterval(function() {
      var goodBtn = $("#simNexts .success button");
      if (goodBtn.length > 0) {
        goodBtn.trigger("click");
      } else {
        FsaController.stopAutoplay();
      }
    }, 800);
  },
  insertState: function(name, cell, hash) {
    hash = hash || name.hashCode();
    FsaController.stateRegistry.insert({
      name: name,
      cell: cell,
      hash: hash
    });
  },
  getStateByHash: function(hash) {
    return FsaController.stateRegistry({
      hash: hash
    }).get()[0];
  },
  getStateByName: function(name) {
    return FsaController.stateRegistry({
      name: name
    }).get()[0];
  },
  getStateByCell: function(cell) {
    var name = FsaController.view.getNameByCell(cell);
    return FsaController.getStateByName(name);
  },
  setMode: function(mode) {
    var success;
    var input;
    var separator;
    if (mode === this.mode.EDIT) {
      this.view.enableEditor();
      if (this.timer) {
        this.stopAutoplay();

      }
    } else if (mode === this.mode.SIM) {
      input = this.view.getInput();
      separator = "";
      success = this.platform.initialize(input.split(separator));
      if (success) {
        var isOk = this.platform.calculate(input);
        this.view.disableEditor(isOk ? true : false);
        return true;
      } else {
        var causa;
        if (this.model.startState) {
          causa = "Az input valószínűleg ismeretlen szimbólumokat tartalmaz.";
        } else {
          causa = "A kezdőállapot nincs beállítva.";
        }
        radio("error").broadcast("Nem lehet elkezdeni a szimulációt: " + causa);
      }
    }
  },
  fetchConfig: function() {
    var aCfg = this.platform.getConfig();
    var chain = this.platform.calculate(aCfg);
    var nRules = this.platform.getApplicableRules(aCfg);
    aCfg.aState = this.getStateByName(aCfg.aState).cell;
    var nCfgs = _u.map(nRules, function(rule) {
      return this.platform.nextConfig(rule.___id);
    }, this);
    this.view.renderSimStep(aCfg, nRules, nCfgs, this.platform.isAcceptable());
    if (!_u.isUndefined(chain[1])) {
      var nextGoodRule = chain[1];
      this.view.hlSimRow(nextGoodRule);
    }
    $(".nextCfg").on("click", function(evt) {
      var rid = evt.currentTarget.id.substr(8);
      FsaController.step(rid);
    });

    //nextBtn bealitas TODO

  },
  step: function(rid) {
    this.platform.execute(rid);
    this.fetchConfig();
  },
  setCommand: function(id) {
    //var id = evt.currentTarget.id;
    this.view.toggleEditBtn(id);
    if (id) {
      this.command = id.substr(0, id.length - 3);
    } else {
      this.command = undefined;
    }
    if (this.command !== "addRule" && !_u.isUndefined(this.addRuleSrc)) {
      this.view.delHLState(this.addRuleSrc);
      this.addRuleSrc = undefined;
    }

    //this.command = command;
  },
  cellClick: function(evt, x, y) {
    //if (x < 0 || x > this.view.sketch.bounds.width || y < 0 || y > this.view.sketch.bounds.height) return false;

    var cell = evt.model;

    var handled = ['delState', 'setFinishState', 'setStartState', 'addRule'];
    if (handled.indexOf(this.command) !== -1 && cell instanceof joint.shapes.fsa.MixState) {
      //var name = (cell instanceof joint.dia.Element ? cell.attributes.attrs.text.text)
      this[this.command](cell);
      //if(this.command)this.setCommand();
    } else if (this.command === "delRule" && cell instanceof joint.shapes.fsa.Arrow) {
      this.delRule(cell);
    }
  },
  blankClick: function(evt, x, y) {
    if (x < 0 || x > this.view.sketch.bounds.width || y < 0 || y > this.view.sketch.bounds.height) return false;
    var handled = ['addState']
    if (handled.indexOf(this.command) !== -1) {
      this[this.command](x, y);
      this.setCommand();
    }
  },
  addState: function(x, y, name) {
    name = name || window.prompt("Új állapot neve: ");
    if (!_u.isNull(name)) {
      var res = this.model.addState(name);
      if (res) {
        var cell = this.view.addState(name, x, y, "delState_" + name.hashCode());
        this.insertState(name, cell, name.hashCode());
        $("#delState_" + name.hashCode()).on("click", function(evt) {
          //var name = evt.currentTarget.id.substr(9);
          FsaController.delState(name); //scope magic.. i hope, it werks...
        });

      } else {
        radio("error").broadcast("Nem lehet hozzáadni a(z) '" + name + "' nevű állapot. Valószínűleg már létezik");
      }
      this.setCommand();
    }

  },
  delState: function(state) {
    var name;
    var cell;
    if (!_u.isString(state)) {
      name = this.view.getNameByCell(state);
      cell = state;
    } else {
      name = state;
      cell = this.getStateByName(name).cell;
    }
    if (this.model.isFinishState(name)) {
      this.setFinishState(name);
    }
    if (this.model.startState === name) {
      this.model.startState = undefined;
    }
    this.model.delState(name);

    this.view.delState(cell, "delState_" + name.hashCode());
    //ki kell torolni az erintett szabajokat is...
    var rules = this.model.rules([{
      aState: name
    }, {
      nState: name
    }]).get();

    _u.each(rules, function(rule) {
      //var graphId = this.ruleRegistry({modelId: rule.___id}).get()[0].graphId;
      this.delRule(rule.___id);
    }, this);
    this.setCommand();
  },
  setFinishState: function(state) {
    var name;
    var cell;
    if (!_u.isString(state)) {
      name = this.view.getNameByCell(state);
      cell = state;
    } else {
      name = state;
      cell = this.getStateByName(name).cell;
    }
    var isFinishState = this.model.finishStates.contains(name);
    if (!isFinishState) {
      var success = this.model.addFinishState(name);
      if (success) {
        this.view.addFinishState(cell, "delFinishState_" + name.hashCode());
        $("#delFinishState_" + name.hashCode()).on("click", function(evt) {
          FsaController.setFinishState(name);
        });
      }

    } else {
      this.model.delFinishState(name);
      this.view.delFinishState(cell, "delFinishState_" + name.hashCode());
    };
    this.setCommand();

  },
  setStartState: function(state) {
    var name;
    var cell;
    if (!_u.isString(state)) {
      name = this.view.getNameByCell(state);
      cell = state;
    } else {
      name = state;
      cell = this.getStateByName(name).cell;
    }
    var oldCell;
    var oldStart = this.model.startState;
    if (oldStart) oldCell = this.getStateByName(oldStart).cell;
    var success = this.model.setStartState(name);
    if (success) {
      this.view.setStartState(cell, oldCell);

    }
    this.setCommand();
  },
  addRule: function(cell, cell2, symbol) {
    if (_u.isUndefined(this.addRuleSrc) && _u.isUndefined(cell2)) {
      this.view.addHLState(cell);
      this.addRuleSrc = cell;
    } else {
      if (!_u.isUndefined(cell2)) {

        this.addRuleSrc = cell;
        cell = cell2;
      }
      symbol = symbol || window.prompt("Szimbolum: ");

      if (!_u.isNull(symbol) && !_u.isUndefined(symbol)) {

        if (!this.model.tape.base.contains(symbol)) {
          if (!this.addSymbol(symbol)) {
            return false;
          }
        }
        var aState = this.view.getNameByCell(this.addRuleSrc);
        var nState = this.view.getNameByCell(cell);
        var id = this.model.addRule(aState, symbol, nState);
        if (id) {
          var cellId;
          var rules = this.model.rules({
            aState: aState,
            nState: nState
          }).get();
          if (rules.length > 1) {
            cellId = this.ruleRegistry({
              modelId: rules[0].___id
            }).get()[0].graphId;
          }
          var labels = _u.pluck(rules, "readSymbol");
          var ruleCell = this.view.addRule(this.addRuleSrc, symbol, cell, "delRule_" + id, "ruleRow_" + id, labels, cellId);
          this.ruleRegistry.insert({
            modelId: id,
            graphId: ruleCell.id
          });
          $("#delRule_" + id).on("click", function(evt) {
            FsaController.delRule(evt.currentTarget.id.substr(8));
          });

        } else {
          radio("error").broadcast("Nem lehet hozzáadni a szabályt. Valószínűleg már létezik");
        }
      }
      this.setCommand();
      return ruleCell ? ruleCell : false;

    }
  },



  delRule: function(rule) {
    var modelId;
    var graphId;
    var cell;
    if (_u.isString(rule)) {
      modelId = rule;
      graphId = this.ruleRegistry({
        modelId: rule
      }).get()[0].graphId;
      cell = this.view.sketch.graph.getCell(graphId);
    } else {
      cell = rule;
      graphId = cell.id;
      modelId = this.ruleRegistry({
        graphId: graphId
      }).get()[0].modelId;
    }
    var mRule = this.model.rules({
      ___id: modelId
    }).get()[0];
    if (mRule) {
      this.model.delRule(mRule.aState, mRule.readSymbol, mRule.nState);
      var labels = this.model.rules({
        aState: mRule.aState,
        nState: mRule.nState
      }).get();
      labels = _u.pluck(labels, "readSymbol");
      this.view.delRule(cell, "ruleRow_" + modelId, labels);
    }
    this.setCommand();
  },

  addSymbol: function(symbol) {
    var succ = this.model.addSymbol(symbol);
    var hash = symbol.hashCode();
    if (succ) {
      this.view.addSymbol(symbol, "delSymbol_" + hash);
      $("#delSymbol_" + hash).on("click", function(evt) {
        FsaController.delSymbol(symbol);
      });
      return true;
    } else {
      radio("error").broadcast("Nem lehet hozzáadni a szimbólumot! Csak egy karakteres szimbólumnevek engedélyezettek.");
      return false;
    }
  },
  delSymbol: function(symbol) {
    var succ = this.model.delSymbol(symbol);
    if (succ) {
      this.view.delSymbol("delSymbol_" + symbol.hashCode());
      var affectedRules = this.model.rules({
        "readSymbol": symbol
      }).get();
      _u.each(affectedRules, function(rule) {
        this.delRule(rule.___id);
      }, this);
    }
  },
  dumpFsa: function() {
    var res = this.model.dump();
    var cells = this.view.sketch.graph.getElements();
    res.states = _u.map(cells, function(cell) {
      return {
        "name": cell.attributes.attrs.text.text,
        "x": cell.attributes.position.x, //el kell tolni, mer a megjelenitoben alapbol el van ennyivel tolva
        "y": cell.attributes.position.y
      };
    });
    var links = this.view.sketch.graph.getLinks();
    res.rules = _u.map(res.rules, function(rule) {
      var modelId = this.model.rules({
        nState: rule.nState,
        aState: rule.aState,
        readSymbol: rule.symbol
      }).get()[0].___id;
      var graphId = this.ruleRegistry({
        modelId: modelId
      }).get()[0].graphId;
      var link = this.view.sketch.graph.getCell(graphId);
      var vertices = link.attributes.vertices;
      rule.vertices = vertices;
      return rule;
    }, this);
    return res;
  },
  loadFsa: function(fsaDefinition) {
    if (_u.isString(fsaDefinition)) {
      try {
        fsaDefinition = JSON.parse(fsaDefinition);
      }catch(e){
        radio("error").broadcast("Nem lehet megnyitni! Érvénytelen véges állapotú automata");
        return false;
      }
    }
    if (_u.isUndefined(fsaDefinition.type) || fsaDefinition.type !== "Fsa") {
      radio("error").broadcast("Nem lehet megnyitni! Érvénytelen véges állapotú automata");
      return false;
    }
    _u.each(fsaDefinition.states, function(state) {
      this.addState(state.x + 20, state.y + 20, state.name);
    }, this);

    _u.each(fsaDefinition.symbols, function(symbol) {
      this.addSymbol(symbol);
    }, this);

    _u.each(fsaDefinition.finishStates, function(state) {
      this.setFinishState(state);
    }, this);

    if (fsaDefinition.startState) this.setStartState(fsaDefinition.startState);
    _u.each(fsaDefinition.rules, function(rule) {
      var aState = this.getStateByName(rule.aState).cell;
      var nState = this.getStateByName(rule.nState).cell;
      var symbol = rule.symbol;
      var link = this.addRule(aState, nState, symbol);
      link.set('vertices', rule.vertices);

    }, this);
  }

};