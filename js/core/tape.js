function Tape(set, bSymbol) {
  if (_u.isUndefined(set)) {
    throw new Error('Tape(): too few arguments. Expected "Tape(set[,blankSymbol])"');
  }
  if (set instanceof Set) {
    this.base = set;
  } else if (typeof set === "function") {
    this.base = {};
    this.base.contains = set;
  } else {
    throw new Error("Argument 1 should be an instance of Set, or a function");
  }
  
  this.blankSymbol = '';
  if (!_u.isUndefined(bSymbol)) {
      this.blankSymbol = bSymbol;
  }
  this.base.add(this.blankSymbol);
  this.data = [this.blankSymbol];
  this.ptr = 0;
}
Tape.LEFT = -1;
Tape.RIGHT = 1;
Tape.HOLD = 0;


Tape.prototype.setBlankSymbol = function(symbol) {
  this.data = _u.map(this.data, function(e) {
    if (e === this.blankSymbol) return symbol;
    else return e;
  }, this);
};

Tape.prototype.movl = function(n) {

  if (_u.isUndefined(n)) n = 1;
  this.ptr = this.ptr - n;
  if (this.ptr < 0) {
    for (var i = this.ptr; i < 0; i++) this.data.unshift(this.blankSymbol);
    this.ptr = 0;
  }

};

Tape.prototype.movr = function(n) {
  
  if (_u.isUndefined(n)) n = 1;
  for (var i = this.ptr + 1; i <= this.ptr + n; i++)
    if (_u.isUndefined(this.data[i])) this.data[i] = this.blankSymbol;
  this.ptr = this.ptr + n;

  if (_u.isUndefined(this.data[this.ptr])) this.data[this.ptr] = this.blankSymbol;
};

Tape.prototype.mov = function(direction, n) {
  if (direction === Tape.RIGHT) {
    this.movr(n);
    return true;
  } else if (direction === Tape.LEFT) {
    this.movl(n);
    return true;
  }else if(direction === Tape.HOLD){
    return true;
  }
  return false;
};
Tape.prototype.erease = function() {
  this.data[this.ptr] = this.blankSymbol;
};

Tape.prototype.read = function() {
  return this.data[this.ptr];
};

Tape.prototype.init = function(arr,ptr) {
  this.data = [this.blankSymbol];
  this.ptr = 0;
  _u.each(arr, function(e) {
    this.write(e);
    this.movr();
  }, this);
  this.ptr = ptr || 0;
};

Tape.prototype.write = function(s) {
  if (this.base.contains(s)) {
    this.data[this.ptr] = s;
  } else {
    throw new TypeError("Tape.write(): Argument 1 should be an element of the base set");
  }
};

Tape.prototype.dump = function() {
  var i, j, diff;
  for (i = 0; this.data[i] === this.blankSymbol; i++);
  for (j = this.data.length - 1; this.data[j] === this.blankSymbol; j--);
  j += 1;
  if (this.ptr < i) {
    i = this.ptr;
  };
  if (this.ptr >= j){
    j = this.ptr+1;
  }
  
  this.data =  this.data.slice(i, j);
  this.ptr -= i; //SIDEEFFECT !!!!!
  return this.data.slice(); 
};