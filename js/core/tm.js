function Tm(tapeNum, names) {
  var generator = new MachineGenerator();
  var i;
  if (_u.isUndefined(names)) {
    names = [];
  }
  tapeNum = (tapeNum > 0) ? tapeNum : 1;
  for (i = 0; i < tapeNum; i += 1) {
    generator.addTape();
    if (_u.isUndefined(names[i])) names[i] = i.toString();
  }
  generator.setTapeNames(names);
  var m = generator.genMachine();

  function TmInstance() {};
  TmInstance.prototype = new m();
  TmInstance.prototype.addRule = function() {
    return this.addFullRule.apply(this, arguments);
  }
  TmInstance.prototype.delRule = function(id){
    this.rules({___id : id}).remove();
    return true;
  }
  TmInstance.prototype.initialize = function(){
    if (_u.isUndefined(this.startState)) {
      throw new Error("Tm.initialize(): Start state is undefined!");
    };
    _u.each(arguments,function(e,i){
      this.tapes[i].init(e);
    },this);
  }
  TmInstance.prototype.dump = function(){
    var res = {};
    res.type = "Tm";
    res.states = this.states.values.slice(0);
    res.finishStates = this.finishStates.values.slice(0);
    res.symbols = _.reject(this.tape.base.values, function(e) {
      return e === ""
    });
    
    res.startState = this.startState;
    res.rules = this.rules().get();
    //res.rules = 
    return res;
  }


  return new TmInstance();
}

function TmPlatform(machine) {
  machine = _u.isUndefined(machine) ? new Tm(1,['']) : machine;
  Platform.call(this, machine);
  this.forceHalt = false;
  this.timeOut = 1000;
}

TmPlatform.prototype = Object.create(Platform.prototype);
/*function haltAfter(n){
  if (_u.isNumber(n){
    this.timeOut = n;
  }
}*/

TmPlatform.prototype.execute = function(ruleId) {
  if (!this.isApplicable(ruleId)) throw new Error("nextConfig(): Can't apply rule");
  var rule = this.machine.rules({
    ___id: ruleId
  }).get()[0];
  _u.each(this.machine.actionKeys, function(e, c) {
    if (c % 2 === 0) {
      this.machine.tapes[parseInt(c / 2)].write(rule[e]);
    }
    if (c % 2 === 1) {
      this.machine.tapes[parseInt(c / 2)].mov(rule[e]);
    }
  }, this);
  this.aState = rule.nState;
  return this.getConfig();

}

TmPlatform.prototype.isAcceptable = function(cfg) {
  if (_u.isUndefined(cfg)) {
    cfg = this.getConfig();
  };
  var isFinishState = this.machine.finishStates.contains(cfg.aState);
  var noRule        = this.getApplicableRules(cfg).length === 0;
  return isFinishState && noRule;
}