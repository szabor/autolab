function Set(crit) {
  "use strict";
  var defaultCrit = function() {
    return true;
  };
  
  if (!_u.isUndefined(crit) && typeof crit === "function") {
    this.crit = crit;
  } else {
    this.crit = defaultCrit;
  }
  this.values = [];

}


Set.union = function(set1, set2) {
  if (set1 instanceof Set && set2 instanceof Set) {
    var res = new Set({
      crit: function(elem) {
        return set1.crit(elem) || set2.crit(elem);
      }
    });
    res.values = _u.sortBy(_u.union(set1.values, set2.values));
    return res;
  } else {
    throw new TypeError("Set.union(): Arguments should be instance of Set");
  }
};

Set.intersect = function(set1, set2) {
  if (set1 instanceof Set && set2 instanceof Set) {
    var res = new Set({
      crit: function(elem) {
        return set1.crit(elem) && set2.crit(elem);
      }
    });
    res.values = _u.sortBy(_u.intersection(set1.values, set2.values));
    return res;
  } else {
    throw new TypeError("Set.intersect(): Arguments should be instance of Set");
  }
};

Set.difference = function(set1, set2) {
  if (set1 instanceof Set && set2 instanceof Set) {
    var res = new Set({
      crit: function(elem) {
        return set1.crit(elem);
      }
    });
    res.values = _u.sortBy(_u.difference(set1.values, set2.values));
    return res;
  } else {
    throw new TypeError("Set.difference(): Arguments should be instance of Set");
  }
};
Set.binaryCartesian = function(set1, set2) {
  if (set1 instanceof Set && set2 instanceof Set) {
    var res = new Set();
    for (var i = 0; i < set1.values.length; i++) {
      for (var j = 0; j < set2.values.length; j++) {
        res.add([set1.values[i],set2.values[j]]);
      }
    }
    return res;
  } else {
    throw new TypeError("Set.difference(): Arguments should be instance of Set");
  }
};

Set.cartesian = function() {
  if (arguments.length > 1) {
    var res = arguments[0];
    for (var i = 1; i < arguments.length; i++) {
      res = Set.binaryCartesian(res,arguments[i]);
    }
    for (var j = 0; j < res.values.length; j++) {
      res.values[j] = _u.flatten(res.values[j]);
    }
    return res;
  } else {
    throw new Error("Set.cartesian(): Too few arguments. At least 2 sets expected");
  }
};

Set.powerset = function(set) {
  throw "NOT IMPLEMENTED";
};

Set.prototype = {

  add: function add(elem) {

    if (this.contains(elem)) return false;
    if (this.crit(elem)) {
      this.values = _u.addSorted(this.values, elem);
      return true;
    } else {
      throw new TypeError("Set.add(): Element " + elem + " does not hold true in criterion...");
    }

  },

  addMore: function addMore(arr) {
    var tmp = arr;
    if (!_u.isArray(arr)) {
      tmp = arguments;
    }
    _u.each(tmp, this.add, this);
  },

  del: function del(elem) {

    if (this.crit(elem)) {
      this.values = _u.delSorted(this.values, elem);
      return true;
    } else {
      throw new TypeError("Set.del(): Element " + elem + " does not hold true in criterion...");
    }
  },

  contains: function contains(elem) {

    if (this.crit(elem)) {
      return _u.indexOf(this.values, elem, true) > -1;
    } else {
      return false; //throw new TypeError("Set.contains(): Element " + elem + " does not hold true in criterion...");
    }
  },

  isSubsetOf: function isSubsetOf(set) {
    if (set instanceof Set) {
      try {
        for (var i = 0; i < this.values.length; i++) {
          if (!set.contains(this.values[i])) return false;
        }
        return true;
      } catch (e) {
        return false;
      }
    } else {
      throw new TypeError("Set.isSubsetOf(): Argument 1 should be an instance of Set");
    }
  },

  isSupersetOf: function isSupersetOf(set) {
    if (set instanceof Set) {
      try {
        for (var i = 0; i < set.values.length; i++) {
          if (!this.contains(set.values[i])) return false;
        }
        return true;
      } catch (e) {
        return false;
      }
    } else {
      throw new TypeError("Set.isSupersetOf(): Argument 1 should be an instance of Set");
    }
  },

  union: function union(set) {
    return Set.union(this, set);
  },

  intersect: function intersect(set) {
    return Set.intersect(this, set);
  },

  difference: function difference(set) {
    return Set.difference(this, set);
  }

};