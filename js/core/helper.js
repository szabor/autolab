_u.mixin({
  /**
   * Hozzaad egy elemet a tombhoz, ugy, hogy rendezett marad.
   * A tombnek rendezettnek kell lennie
   * @PARAM arr : a tomb, amibe beszurunk(nem valtozik)
   * @PARAM val : az ertek, amit beszurunk
   * @RETURN    : a 'val' elemet tartalmazo rendezett tomb.
   */
  'addSorted': function(arr, val) {
    var index = _u.sortedIndex(arr, val);
    return _u.first(arr, index).concat([val], _u.rest(arr, index));
  },
  /**
   * Torol egy elemet(az elso elofordulasat) egy rendezett tombhoz.
   * A tombnek rendezettnek kell lennie.
   *
   * @PARAM arr : a tomb, amibol torlunk(nem valtozik)
   * @PARAM val : az ertek, amit torlunk
   * @RETURN    : a 'val' elemet nem tartalmazo rendezett tomb.
   */
  'delSorted': function(arr, val) {
    var index = _u.indexOf(arr, val, true);
    var res = arr;
    if (index >= 0) {
      res = _u.first(arr, index).concat(_u.rest(arr, index + 1));
    }
    return res;

  },
  'charRange' : function(from, to, step){
    from = from.charCodeAt(0);
    to = to.charCodeAt(0)+1;
    return _u.map(_u.range(from,to,step),function(e){
      return String.fromCharCode(e);
    });
  },
  /**
   * Objektumok, tombok masolasa
   *
   */
  'clone': function (obj) {
    if (null === obj || "object" != typeof obj) return obj;
    var copy;
    
    if (obj instanceof Date) {
        copy = new Date();
        copy.setTime(obj.getTime());
        return copy;
    }

    if (obj instanceof Array) {
        copy = [];
        for (var i = 0, len = obj.length; i < len; i++) {
            copy[i] = _u.clone(obj[i]);
        }
        return copy;
    }

    if (obj instanceof Object) {
        copy = {};
        for (var attr in obj) {
            if (obj.hasOwnProperty(attr)) copy[attr] = _u.clone(obj[attr]);
        }
        return copy;
    }

    throw new Error("Unable to copy obj! Its type isn't supported.");
}

});