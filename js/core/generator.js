function MachineGenerator() {
  this.tapes = [];
  this.states = new Set(function(e) {
    return _u.isString(e);
  });
  this.finishStates = new Set(function(e) {
    return _u.isString(e); // TODO valahogy hozzakotni a states-hez
  });
}
MachineGenerator.prototype.addTape = function(set, blankSymbol) {
  //name = _u.isUndefined(name) ? this.getNextTapeName() : name;
  set = _u.isUndefined(set) ? new Set() : set;
  /*if (_u.has(this.tapes, name)) {
    throw new Error('Machine.addTape(): Name ' + name + ' is already taken...');
  }*/
  //this.tapes[name] = new Tape(set,blankSymbol);
  var tape = new Tape(set, blankSymbol);
  this.tapes.push(new Tape(set, blankSymbol));
};

MachineGenerator.prototype.delTape = function(num) {
  delete this.tapes[num];
};
MachineGenerator.prototype.setTapeNames = function(names) {
  if (names.length === this.tapes.length) {
    this.tapeNames = names;
  } else {
    throw new Error("Tapenames...");
  }
}

/*MachineGenerator.prototype.getNextTapeName = function() {
  var len = Object.keys(this.tapes).length + 1;
  return "tape_" + len;
};*/

MachineGenerator.prototype.genMachine = function() {
  var tapes = this.tapes;
  var states = this.states;
  var finishStates = this.finishStates;
  //var tapeNames = this.tapeNames;
  var tapeNames = _u.map(this.tapeNames, function(e) {
    return _u.map(_u.filter(e.split(" "), function(e) {
      var reg = /[A-Za-z][A-Za-z0-9]*/;
      var matches = e.match(reg);
      return _u.isNull(matches) ? false : matches[0] === e
    }), function(e) {
      return e.charAt(0).toUpperCase() + e.slice(1);
    }).join("");
  });

  function Machine() {
    var self = this;
    self.tapes = tapes;
    self.rules = TAFFY();
    self.states = new Set(function(e) {
      return _u.isString(e);
    });
    self.finishStates = new Set(function(e) {
      return self.states.contains(e);
    });
    self.ruleKeys = ['aState'];
    _u.each(tapeNames, function(e) {
      this.ruleKeys.push('read' + e + 'Symbol');
    }, self);
    self.ruleKeys.push('nState');
    _u.each(tapeNames, function(e) {
      this.ruleKeys.push('write' + e + 'Symbol');
      this.ruleKeys.push('move' + e);
    }, self);

    self.configKeys = self.ruleKeys.slice(1, self.tapes.length + 1);
    self.actionKeys = self.ruleKeys.slice(self.tapes.length + 2);

    return self;
  }

  _u.each(tapeNames, function(name, index) {

    Machine.prototype['tape' + name] = tapes[index];
    Machine.prototype['add' + name + 'Symbol'] = function(symbol) {
      return this.tapes[index].base.add(symbol);
    };
    Machine.prototype['del' + name + 'Symbol'] = function(symbol) {
      if (symbol !== 'eps' && symbol !== this.tapes[index].blankSymbol) {
        return this.tapes[index].base.del(symbol);
      } else {
        return false;
      }
    }
  });

  Machine.prototype.addState = function(state) {
    return this.states.add(state);
  };

  Machine.prototype.delState = function(state) {
    return this.states.del(state);
  };

  Machine.prototype.isFinishState = function(state) {
    return this.finishStates.contains(state);
  };

  Machine.prototype.addFinishState = function(state) {
    return this.finishStates.add(state);
  };

  Machine.prototype.delFinishState = function(state) {
    return this.finishStates.del(state);
  };

  Machine.prototype.setStartState = function(state) {
    if (this.states.contains(state)) {
      this.startState = state;
      return true;
    } else {
      throw new Error("Start state should be an element of states");
    }
  };

  /**
   *
   *
   */
  Machine.prototype.addFullRule = function() {
    arguments = Array.prototype.slice.call(arguments);
    var rule = {};
    //ERROR CHECKING

    //Az argumentumok: aState,[readTapeSymbol]+,nState, [writeTapeSymbols,movDirection]+
    if (arguments.length !== this.ruleKeys.length) throw new Error("addFullRule(): Argument list size differs from expected.");

    //aState
    if (!this.states.contains(arguments[0])) throw new Error("addFullRule(): state '" + arguments[0] + "' does not exists.");

    //olvasott szimbolumok
    _u.each(arguments.slice(1, this.tapes.length + 1), function(s, i) {
      if (!this.tapes[i].base.contains(s)) {
        throw new Error("addFullRule(): symbol '" + s + "' is not an element of the " + i + ". tape's base set.");
      };
    }, this);

    //nState
    if (!this.states.contains(arguments[this.tapes.length + 1])) throw new Error("addFullRule(): state '" + arguments[this.tapes.length + 1] + "' does not exists.");

    //szimbolumok irasra + mozgas
    var maradek = _u.partition(arguments.slice(this.tapes.length + 2), function(e, i) {
      if (i % 2 == 0) {
        return true;
      } else {
        return false;
      }
    });
    //kimeno szimbolumok
    _u.each(maradek[0], function(e, c) {
      if (_u.isArray(e)) { // tobbet kell egyszerre irni...
        var ok = _u.every(e, function(elem) {
          return this.tapes[c].base.contains(elem);
        }, this);
        if (!ok) {
          throw new Error("addFullRule(): symbol '" + elem + "' is not an element of the " + c + ". tape's base set.");
        }
      } else {
        if (!this.tapes[c].base.contains(e)) {
          throw new Error("addFullRule(): symbol '" + e + "' is not an element of the " + c + ". tape's base set.");
        }

      }
    }, this);

    //iranyok
    var dirsValid = _u.every(maradek[1], function(e) {
      return (e === Tape.RIGHT || e === Tape.LEFT || e === Tape.HOLD);
    });
    if (!dirsValid) {
      throw new Error("addFullRule(): Invalid direction!");
    }
    var obj = _u.object(this.ruleKeys, arguments);
    if (_u.isEmpty(this.rules(obj).get())) {

      var res = this.rules.insert(_u.object(this.ruleKeys, arguments));
      return res.get()[0].___id;

    }
    return false;

  };
  Machine.prototype.delFullRule = function() {
    arguments = Array.prototype.slice.call(arguments);
    /*var rule = {};
    //ERROR CHECKING
    if (arguments.length !== this.ruleKeys.length) throw new Error("addFullRule(): Argument list size differs from expected.");

    if (!this.states.contains(arguments[0])) throw new Error("addFullRule(): state '" + arguments[0] + "' does not exists.");

    _u.each(arguments.slice(1, this.tapes.length + 1), function(s, i) {
      if (!this.tapes[i].base.contains(s)) {
        throw new Error("addFullRule(): symbol '" + s + "' is not an element of the " + i + ". tape's base set.");
      };
    }, this);

    if (!this.states.contains(arguments[this.tapes.length + 1])) throw new Error("addFullRule(): state '" + arguments[this.tapes.length + 1] + "' does not exists.");

    _u.each(arguments.slice(this.tapes.length + 2), function(e, i) {
      if (i % 2 == 0 && !this.tapes[parseInt(i / 2)].base.contains(e)) {
        throw new Error("addFullRule(): symbol '" + e + "' is not an element of the " + i + ". tape's base set.");
      };
      if (i % 2 == 1 && (e !== Tape.RIGHT && e !== Tape.LEFT && e !== Tape.HOLD)) {
        throw new Error("addFullRule(): wrong direction: '" + e.movDir + "'.");
      };
    }, this);*/

    this.rules(_u.object(this.ruleKeys, arguments)).remove();
  }
  return Machine;

};