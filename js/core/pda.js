function Pda() {
  var generator = new MachineGenerator();

  var inSymbols = new Set(function(e) {
    return _u.isString(e);
  });
  var stackSymbols = new Set(function(e) {
    return _u.isString(e);
  });

  inSymbols.add('eps');
  stackSymbols.add('eps');
  generator.addTape(inSymbols, 'eps');
  generator.addTape(stackSymbols, 'eps');
  generator.setTapeNames(['', 'stack']);
  var m = generator.genMachine();

  function PdaInstance() {};
  PdaInstance.prototype = new m();

  PdaInstance.prototype.addRule = function(aState, aSymbol, aStackSymbol, nState, toPush) {
    var inputDir = (aSymbol === "eps") ? Tape.HOLD : Tape.RIGHT;
    if (!_u.isArray(toPush)) toPush = [toPush];
    return this.addFullRule(aState, aSymbol, aStackSymbol, nState, "eps", inputDir, toPush, Tape.HOLD);
  };
  PdaInstance.prototype.delRule = function(id) {
    this.rules({___id : id}).remove();
  };
  PdaInstance.prototype.setStartStackSymbol = function(ss) {
    this.tapeStack.base.add(ss);
    this.startStackSymbol = ss;
  }

  PdaInstance.prototype.initialize = function(input, startStackSymbol) {
    if (!_u.isUndefined(startStackSymbol)) {
      this.setStartStackSymbol(startStackSymbol);
    };
    if (_u.isUndefined(this.startStackSymbol)) {
      throw new Error("Pda.initialize(): Start stack symbol is undefined!");
    };
    if (_u.isUndefined(this.startState)) {
      throw new Error("Pda.initialize(): Start state is undefined!");
    };
    this.tapeStack.init([this.startStackSymbol]);
    this.tape.init(input);
  }
  PdaInstance.prototype.setMode = function(m) {

    if (m === Pda.EMPTYMODE || m === Pda.FSMODE) {
      this.mode = m
    } else {
      throw new TypeError("Pda.setMode(): Argument 1 should be Pda.EMPTYMODE or Pda.FSMODE");
    }
  }
  PdaInstance.prototype.dump = function(){
    var res = {};
    res.type = "Pda";
    res.states = this.states.values.slice(0);
    res.finishStates = this.finishStates.values.slice(0);
    res.symbols = _.reject(this.tape.base.values, function(e) {
      return e === "eps"
    });
    res.stackSymbols = _.reject(this.tapeStack.base.values,function(e){
      return e=== "eps";
    });
    res.mode = (this.mode === Pda.EMPTYMODE ? "empty" : (this.mode === Pda.FSMODE ? "fs" : ""));
    res.startState = this.startState;
    var rules = this.rules().get();
    res.rules = _u.map(rules, function(rule) {
      return {
        aState: rule.aState,
        symbol: rule.readSymbol,
        stackSymbol: rule.readStackSymbol,
        nState: rule.nState,
        toPush: rule.writeStackSymbol
      };
    }, res);
    return res;
  }
  var ret = new PdaInstance();
  //ret.setStartStackSymbol('#');
  return ret;
  //return m;
}
Pda.EMPTYMODE = {};
Pda.FSMODE = {};

function PdaPlatform(machine) {
  machine = _u.isUndefined(machine) ? new Pda() : machine;
  Platform.call(this, machine);
}
PdaPlatform.prototype = Object.create(Platform.prototype);
PdaPlatform.prototype.execute = function(ruleId) {
  if (!this.isApplicable(ruleId)) throw new Error("PdaPlatform.execute(): Can't apply rule");
  var rule = this.machine.rules({
    ___id: ruleId
  }).get()[0];
  if (rule.readSymbol !== "eps") { // epszilon- input szalag
    this.machine.tape.movr();
  }

  if (rule.readStackSymbol !== "eps") {
    this.machine.tapeStack.write(this.machine.tapeStack.blankSymbol);
    this.machine.tapeStack.movl();
  }

  _u.each(rule.writeStackSymbol, function(e) {
    if (e !== "eps") {
      this.machine.tapeStack.movr();
      this.machine.tapeStack.write(e);
    }
  }, this);
  this.aState = rule.nState;
  return this.getConfig();
}

PdaPlatform.prototype.isAcceptable = function(cfg) {
  if (_u.isUndefined(cfg)) {
    cfg = this.getConfig();
  }
  var condition = false;
  if (this.machine.mode === Pda.EMPTYMODE) {
    condition = ((cfg.tapes[1].ptr === 0) &&
      (cfg.tapes[1].data[0] === this.machine.startStackSymbol));
  } else if (this.machine.mode === Pda.FSMODE) {
    condition = this.machine.finishStates.contains(cfg.aState);
  }

  return ("eps" === cfg.tapes[0].data[cfg.tapes[0].ptr] && condition);
}