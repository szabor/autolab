function Platform(machine) {
  this.machine = machine;
  this.symbolSeparator = '';
  this.aState = machine.startState;
  this.HALTNOW = false;


};

Platform.prototype.getConfig = function() {
  var res = {};
  res.aState = this.aState;
  res.tapes = _u.map(this.machine.tapes, function(e) {
    return {
      data: e.dump(),
      ptr: e.ptr,
    };
  });
  return res;
};

Platform.prototype.setConfig = function(cfg) {
  this.aState = cfg.aState;
  _u.each(cfg.tapes, function(e, i) {
    this.machine.tapes[i].init(e.data, e.ptr);
  }, this);
};

Platform.prototype.getApplicableRules = function(cfg) {
  cfg = cfg || this.getConfig();
  //var filter = {aState : cfg.aState};
  var filter = _u.map(cfg.tapes, function(e) {
    return [e.data[e.ptr], "eps"];
  });
  var filter = _u.object(this.machine.configKeys, filter);
  filter.aState = cfg.aState;
  return this.machine.rules(filter).get();
};

Platform.prototype.isApplicable = function(ruleId, cfg) {
  var cfg = cfg || this.getConfig();
  var rule = this.machine.rules({
    ___id: ruleId
  }).get()[0];
  var configKeys = this.machine.ruleKeys.slice(1, this.machine.tapes.length);
  if (cfg.aState !== rule.aState) return false;
  return _u.every(configKeys, function(e, i) {
    return (rule[e] === "eps" || rule[e] === cfg.tapes[i].data[cfg.tapes[i].ptr]);
  }, this);
};

Platform.prototype.nextConfig = function(ruleId, cfg) {
  //if(!this.isApplicable(ruleId)) throw new Error("nextConfig(): Can't appli rule");
  var aCfg = this.getConfig();
  if (cfg) this.setConfig(cfg);
  var nCfg = this.execute(ruleId);
  this.setConfig(aCfg); // visszaallitjuk az eredetit...
  return nCfg;
};

Platform.prototype.execute = function(ruleId) {
  if (!this.isApplicable(ruleId)) throw new Error("Platform.execute(): Can't apply rule");
  var rule = this.machine.rules({
    ___id: ruleId
  }).get()[0];
  _u.each(this.machine.actionKeys, function(e, c) {
    if (c % 2 === 0) { //write
      if (_u.isArray(rule[e])) {
        //tobb szimbolum irasa egyszerre...
        var writeCount = 0;
        _u.each(rule[e], function(e) {
          if (e !== "eps") {
            writeCount += 1;
            this.machine.tapes[parseInt(c / 2)].write(e);
            this.machine.tapes[parseInt(c / 2)].mov(Tape.RIGHT);
          }
        }, this);
        if (writeCount > 0) this.machine.tapes[parseInt(c / 2)].mov(Tape.LEFT);
      } else {
        if (rule[e] !== "eps") this.machine.tapes[parseInt(c / 2)].write(rule[e]);
      };

    }
    if (c % 2 === 1) { //move
      this.machine.tapes[parseInt(c / 2)].mov(rule[e]);
    }
  }, this);
  this.aState = rule.nState;
  return this.getConfig();
};
Platform.prototype.initialize = function() {
  try {
    this.machine.initialize.apply(this.machine, arguments);
    this.aState = this.machine.startState;
    return true;
  } catch (e) {
    return false;
  }


}
Platform.prototype.setSymbolSeparator = function(sep) {
  this.symbolSeparator = sep;
}
Platform.prototype.genTrace = function(tree, indexes, last) {
  var res = [];
  //ptr = last;
  while (last > 0) {
    res.unshift(tree[last]);
    last = indexes[last - 1];
  }
  res.unshift(tree[0]);
  return res;
}
Platform.prototype.calculate = function(input,stopAt) {
  if (_u.isUndefined(input)) {
    throw new Error("Platform.calculate(): Missing argument");
  }
  if (_u.isString(input)) { // string... megprobaljuk szetszedni...
    input = input.split(this.symbolSeparator);
  }
  if (_u.isArray(input)) { //rendes input
    this.initialize(input);
  } else {
    try {
      this.setConfig(input);
    } catch (e) {
      throw new Error("Platform.calculate(): Argument 1 must be an input string or a valid configuration");
    }
  }
  var initCfg;
  var aCfg; // aktualis konfiguracio
  var aRules; // aktualisan alkalmazhato szabalyok
  var nCfgs; // kovetkezo lehetseges konfiguraciok
  var trace; // trace 
  var cfgQueue = [];
  var ruleTree = ['*'];
  var cfgCount = 0;
  var chain = [];
  var allCfg = [];
  allCfg.contains = function(obj) {
    var res = _u.find(this, function(e) {
      return (JSON.stringify(obj) === JSON.stringify(e));
    });
    if (_u.isUndefined(res)) return false;
    return true;
  }



  initCfg = this.getConfig();
  allCfg.push(initCfg);
  cfgQueue.push(initCfg);

  do {
    if(stopAt && cfgCount % 100 === 0){
      var now = new Date().getTime();
      if(now > stopAt){
        if(radio) radio("error").broadcast("Az automata túl sokáig futott.");
        return false;
      }
    }
    if (cfgQueue.length === 0) { // nincs mit kifejteni...:(
      return false;
    }
    aCfg = cfgQueue.shift();
    if (this.isAcceptable(aCfg)) {
      return this.genTrace(ruleTree, chain, cfgCount);
    } else {
      aRules = this.getApplicableRules(aCfg);
      nCfgs = _u.map(aRules, function(e) { // kovetkezo konfiguraciok
        return this.nextConfig(e.___id, aCfg);
      }, this);

      _u.each(nCfgs, function(e, index) {
        if (!allCfg.contains(e)) { // ha meg nem volt kifejtve
          allCfg.push(e); // betesszuk a tobbi koze
          cfgQueue.push(e); // meg a kifejtenivalok koze
          ruleTree.push(aRules[index].___id); //es a szabajt is elmentjuk
          chain.push(cfgCount);
        }
      }, this);
      cfgCount += 1;
    }
  } while (cfgQueue.length > 0);
  return false;

}