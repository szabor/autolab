
function Fsa(){
  var generator = new MachineGenerator();

  var inSymbols = new Set(function(e){return _u.isString(e) && (e.length === 1 || e === "eps");});
  //inSymbols.add('eps');
  generator.addTape(inSymbols,'eps');
  generator.setTapeNames(['']);
  var m = generator.genMachine();

  function FsaInstance() {};
  FsaInstance.prototype = new m();
  FsaInstance.prototype.addRule = function(aState,aSymbol,nState){
    var direction = (aSymbol === "eps") ? Tape.HOLD : Tape.RIGHT;
    this.tapes[0].base.add(aSymbol);
    return this.addFullRule(aState,aSymbol,nState,"eps",direction);
    
  };
  FsaInstance.prototype.delRule = function(aState,aSymbol,nState){
    var direction = (aSymbol === "eps") ? Tape.HOLD : Tape.RIGHT;
    this.delFullRule(aState,aSymbol,nState,"eps",direction);
  };
  FsaInstance.prototype.initialize = function(input){
    if (_u.isUndefined(this.startState)) {
      throw new Error("Fsa.initialize(): Start state is undefined!");
    };
    //this.tapeStack.init([this.startStackSymbol]);
    this.tape.init(input);
  }
  FsaInstance.prototype.dump = function(){
    var res = {};
    res.type = "Fsa";
    res.states = this.states.values.slice(0);
    res.finishStates = this.finishStates.values.slice(0);
    res.symbols = _.reject(this.tape.base.values,function(e){return e === "eps"});

    res.startState = this.startState;
    var rules = this.rules().get();
    res.rules = _u.map(rules,function(rule){
      return {aState : rule.aState, symbol : rule.readSymbol, nState : rule.nState};
    },res);
    return res;

  }

  return new FsaInstance();
  //return m;
}

function FsaPlatform(machine){
  machine = _u.isUndefined(machine)? new Fsa() : machine;
  Platform.call(this,machine);
}
FsaPlatform.prototype = Object.create(Platform.prototype);

FsaPlatform.prototype.isAcceptable = function(cfg){
  if(_u.isUndefined(cfg)){
    cfg = this.getConfig();
  }
  return (this.machine.finishStates.contains(cfg.aState) && this.machine.tapes[0].blankSymbol === cfg.tapes[0].data[cfg.tapes[0].ptr]);
}




