/*! JointJS v0.8.1 - JavaScript diagramming library  2014-02-24 


This Source Code Form is subject to the terms of the Mozilla Public
License, v. 2.0. If a copy of the MPL was not distributed with this
file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */
if (typeof exports === 'object') {

    var joint = {
        util: require('../src/core').util,
        shapes: {
            basic: require('./joint.shapes.basic')
        },
        dia: {
            Element: require('../src/joint.dia.element').Element,
            Link: require('../src/joint.dia.link').Link
        }
    };
}

joint.shapes.fsa = {};


joint.shapes.fsa.MixState = joint.shapes.basic.Circle.extend({
    markup: '<g class="rotatable"><g class="scalable"><circle class="outer"/><circle class="inner"/></g><text/></g>',
    defaults: joint.util.deepSupplement({
        type: 'fsa.MixState',
        attrs: {
            '.outer': {
                transform: 'translate(10, 10)',
                r: 14,
                'stroke-width' : 2,
                fill: '#FFFFFF',
                stroke: 'black'
            },

            '.inner': {
                transform: 'translate(10, 10)',
                r: 12,
                stroke: 'white',
                'stroke-width' : 2,
                fill: '#FFFFFF'
            },
            'text': { 'font-size': 18,transform: 'translate(10, 10)', text: '', 'text-anchor': 'middle', 'ref-x': .5, 'ref-y': .5, ref: '.inner', 'y-alignment': 'middle', fill: 'black', 'font-family': 'Arial, helvetica, sans-serif' }
        
        }
    }, joint.dia.Element.prototype.defaults)
});

joint.shapes.fsa.Arrow = joint.dia.Link.extend({

    defaults: joint.util.deepSupplement({
	type: 'fsa.Arrow',
        attrs: { '.marker-target': { d: 'M 10 0 L 0 5 L 10 10 z' }},
        smooth: true
    }, joint.dia.Link.prototype.defaults)
});

if (typeof exports === 'object') {

    module.exports = joint.shapes.fsa;
}
